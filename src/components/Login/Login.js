import React from 'react';
import { Form, Icon, Input, Button, /*Checkbox*/ } from 'antd';
import './Login.css';
import axios from 'axios';
import { saveToken } from '../../config';

const FormItem = Form.Item;

class NormalLoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.reset = {
        username: "",
        password: "",
    };
    this.state = {
        username: "",
        password: "",
    };
  }

  handleInputChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleSubmit = (e) => {
    e.preventDefault();

    const loginObject = {
        username: this.state.username,
        password: this.state.password,
    };

    axios.post(`https://guarded-headland-70198.herokuapp.com/api-token-auth/`, loginObject)
        .then((res) => {
            console.log('api-token-auth res object:', res);
            const token = res.data.token;
            saveToken(token);
            this.props.sendToken(token);
            this.setState({ ...this.reset });
            // this.props.history.push('/cards');
        })
        .catch(err => {
            console.log("submitAndLogin ERROR:",err);
            if (err.response) {
              alert(`Login unsuccessful. Please try again.\n${err.response.status}: ${err.response.statusText}`);
            } else {
              alert(`Login unsuccessful.\n${err}`);
            }
          });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
        <FormItem>
          {getFieldDecorator('userName', {
            rules: [{ required: true, message: 'Please input your username!' }],
          })(
            <Input 
              name="username" 
              onChange={this.handleInputChange} 
              value={this.state.username} 
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} 
              placeholder="Username" />
          )}
        </FormItem>
        <FormItem>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please input your Password!' }],
          })(
            <Input 
              name="password" 
              onChange={this.handleInputChange} 
              value={this.state.password} 
              prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} 
              type="password" 
              placeholder="Password" />
          )}
        </FormItem>
        <FormItem>
          {/* {getFieldDecorator('remember', {
            valuePropName: 'checked',
            initialValue: true,
          })(
            <Checkbox>Remember me</Checkbox>
          )}
          <a className="login-form-forgot" href="">Forgot password</a> */}
          <Button type="primary" htmlType="submit" className="login-form-button">
            Log in
          </Button>
          {/* Or <a href="">register now!</a> */}
        </FormItem>
      </Form>
    );
  }
}

export default Form.create()(NormalLoginForm);

