import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { BrowserRouter as Router } from 'react-router-dom';
import registerServiceWorker from './registerServiceWorker';
// import '../node_modules/antd/dist/antd.css';
require('dotenv').config()

ReactDOM.render(
<Router>
    <App />
</Router>, document.getElementById('root'));
registerServiceWorker();
