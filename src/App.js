import React, { Component } from 'react';
import Login from './components/Login/Login';
import { Button } from 'antd';
import { Link, Route, Switch } from 'react-router-dom';
import './App.css';

class App extends Component {
  constructor() {
    super();
    this.state = {
      token: '',
    }
  }

  saveToken = (token) => {
    this.setState({ token });
  }

  render() {
    return (
      <div className="app">
        <div className="appHeader">
          <h1>Welcome to my Janky Card Database!</h1>
          <h3>Prepare for the jankiest s***t you've ever seen.</h3>
        </div>
        <Switch>
          <Route exact path='/'><Link to='/login'><Button type='dashed'>Log in!</Button></Link></Route>
          <Route path='/login' render={(props) => <Login {...props} sendToken={this.saveToken} />} />
        </Switch>

      </div>
    );
  }
}

export default App;
